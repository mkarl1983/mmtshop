﻿using MMTShop.API.Controllers;
using MMTShop.Application.Features.Categories.Commands.Create;
using MMTShop.Application.Features.Categories.Commands.Delete;
using MMTShop.Application.Features.Categories.Commands.Update;
using MMTShop.Application.Features.Categories.Queries.GetAll;
using MMTShop.Application.Features.Categories.Queries.GetById;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MMTShop.Api.Controllers.v1
{
    public class CategoryController : BaseApiController<CategoryController>
    {

        [HttpGet("GetCategoryById")]
        public async Task<IActionResult> GetById(int id)
        {
            var category = await _mediator.Send(new GetCategoryByIdQuery() { Id = id });
            return Ok(category);
        }

        // POST api/<controller>
        [HttpPost("CreateCategory")]
        public async Task<IActionResult> Post(CreateCategoryCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        // PUT api/<controller>/5
        [HttpPut("UpdateCategory")]
        public async Task<IActionResult> Put(int id, UpdateCategoryCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await _mediator.Send(command));
        }

        // DELETE api/<controller>/5
        [HttpDelete("DeleteCategory")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _mediator.Send(new DeleteCategoryCommand { Id = id }));
        }
    }
}