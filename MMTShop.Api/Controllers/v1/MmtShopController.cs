﻿using MMTShop.API.Controllers;
using MMTShop.Application.Features.Categories.Commands.Create;
using MMTShop.Application.Features.Categories.Commands.Delete;
using MMTShop.Application.Features.Categories.Commands.Update;
using MMTShop.Application.Features.Categories.Queries.GetAll;
using MMTShop.Application.Features.Categories.Queries.GetById;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MMTShop.Application.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;
using MMTShop.Application.Common;
using MMTShop.Application.Features.Products.Queries.GetFeaturedProducts;
using System.Collections.Generic;

namespace MMTShop.Api.Controllers.v1
{
    public class MmtShopController : BaseApiController<MmtShopController>
    {

        [HttpGet("FeaturedProducts")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<List<GetFeaturedProductsResponse>>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetFeaturedProducts()
        {
            var featuredProducts = await _mediator.Send(new GetFeaturedProductsQuery());
            return Ok(featuredProducts);
        }

        [HttpGet("AvailableCategories")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<List<GetAllCategoryResponse>>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAll()
        {
            var categories = await _mediator.Send(new GetAllCategoryQuery());
            return Ok(categories);
        }

        [HttpGet("ProductsByCategory")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<List<GetProductsByCategoryResponse>>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductsByCategory(int id)
        {
            var product = await _mediator.Send(new GetProductsByCategoryQuery() { Id = id });
            return Ok(product);
        }
    }
}