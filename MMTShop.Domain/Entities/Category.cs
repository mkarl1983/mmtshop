﻿

using MMTShop.Domain.Abstractions;

namespace MMTShop.Domain.Entities
{
    public class Category : AuditableEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}