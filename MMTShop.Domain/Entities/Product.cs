﻿

using MMTShop.Domain.Abstractions;

namespace MMTShop.Domain.Entities
{
    public class Product : AuditableEntity
    {
        public string Name { get; set; }
        public string Sku { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}