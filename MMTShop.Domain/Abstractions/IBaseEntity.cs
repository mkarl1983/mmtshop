﻿namespace MMTShop.Domain.Abstractions
{
    public interface IBaseEntity
    {
        public int Id { get; set; }
    }
}