﻿using MMTShop.Application.Interfaces.CacheRepositories;
using MMTShop.Application.Interfaces.Repositories;
using MMTShop.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using MMTShop.Application.Common.ThrowR;

namespace MMTShop.Infrastructure.CacheRepositories
{
    public class ProductCacheRepository : IProductCacheRepository
    {
        private readonly IProductRepository _productRepository;

        public ProductCacheRepository( IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<Product> GetByIdAsync(int productId)
        {

            // implement cachd version here i.e reterive from cache and if null reterive from actual 
            var product =  await _productRepository.GetByIdAsync(productId);
            Throw.Exception.IfNull(product, "Product", "No Product Found");
            return product;
        }

        public async Task<List<Product>> GetCachedListAsync()
        {
            // implement cachd version here i.e reterive from cache and if null reterive from actual 
            var productList  = await _productRepository.GetListAsync();
            return productList;
        }

        public async Task<List<Product>> GetCachedFeaturedProductListAsync()
        {
            // implement cachd version here i.e reterive from cache and if null reterive from actual 
            var productList = await _productRepository.GetFeaturedProductsAsync();
            return productList;
        }

        public async Task<List<Product>> GetCachedProductsByCategoryListAsync(int categoryId)
        {
            var productList = await _productRepository.GetProductsByCatgoryAsync(categoryId);
            return productList;
        }
    }
}