﻿using MMTShop.Application.Interfaces.CacheRepositories;
using MMTShop.Application.Interfaces.Repositories;
using MMTShop.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using MMTShop.Application.Common.ThrowR;

namespace MMTShop.Infrastructure.CacheRepositories
{
    public class CategoryCacheRepository : ICategoryCacheRepository
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryCacheRepository(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Category> GetByIdAsync(int categoryId)
        {

            var category = await  _categoryRepository.GetByIdAsync(categoryId);
            Throw.Exception.IfNull(category, "Category", "No Category Found");
            return category;
        }

        public async Task<List<Category>> GetCachedListAsync()
        {

            var categoryList = await _categoryRepository.GetListAsync();
            return categoryList;
        }
    }
}