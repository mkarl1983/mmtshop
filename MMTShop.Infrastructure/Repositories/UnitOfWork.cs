﻿using MMTShop.Application.Interfaces.Repositories;
using MMTShop.Application.Interfaces.Shared;
using MMTShop.Infrastructure.DbContexts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MMTShop.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public ApplicationDbContext _dbContext;
        private bool disposed;

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<int> Commit(CancellationToken cancellationToken)
        {
            return await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public Task Rollback()
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    //dispose managed resources
                    _dbContext.Dispose();
                }
            }
          
            disposed = true;
        }

    }
}