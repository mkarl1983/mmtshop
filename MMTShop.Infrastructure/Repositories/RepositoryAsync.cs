﻿using MMTShop.Application.Interfaces.Repositories;
using MMTShop.Infrastructure.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMTShop.Infrastructure.Repositories
{
    public class RepositoryAsync<T> : IRepositoryAsync<T> where T : class
    {
        private readonly ApplicationDbContext _dbContext;

        public RepositoryAsync(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<T> Entities => _dbContext.Set<T>();

        public async Task<T> AddAsync(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);
            return entity;
        }

        public Task DeleteAsync(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            return Task.CompletedTask;
        }

        public async Task<List<T>> GetAllAsync()
        {
            return await _dbContext
                .Set<T>()
                .ToListAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task<List<T>> GetPagedReponseAsync(int pageNumber, int pageSize)
        {
            return await _dbContext
                .Set<T>()
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task UpdateAsync(T entity)
        {
            _dbContext.Entry(entity).CurrentValues.SetValues(entity);
            return Task.CompletedTask;
        }


        ////When you expect a model back (async)
        //public async Task<int> ExecWithStoreProcedureAsync(string query, params object[] parameters)
        //{
        //    return await _dbContext.Database.ExecuteSqlRawAsync(query, parameters);
        //}

        ////When you expect a model back
        //public int ExecWithStoreProcedure(string query)
        //{
        //    return _dbContext.Database.ExecuteSqlRaw(query);
        //}

        //// Fire and forget (async)
        //public async Task ExecuteWithStoreProcedureAsync(string query, params object[] parameters)
        //{
        //    await _dbContext.Database.ExecuteSqlRawAsync(query, parameters);
        //}

        //// Fire and forget
        //public void ExecuteWithStoreProcedure(string query, params object[] parameters)
        //{
        //    _dbContext.Database.ExecuteSqlRawAsync(query, parameters);
        //}
    }
}