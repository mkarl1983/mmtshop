﻿using MMTShop.Application.Interfaces.Repositories;
using MMTShop.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MMTShop.Infrastructure.DbContexts;

namespace MMTShop.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IRepositoryAsync<Product> _repository;
        private readonly ApplicationDbContext _dbContext;

        public ProductRepository( IRepositoryAsync<Product> repository, ApplicationDbContext dbContext)
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        public IQueryable<Product> Products => _repository.Entities;

        public async Task DeleteAsync(Product product)
        {
            await _repository.DeleteAsync(product);
        }

        public async Task<Product> GetByIdAsync(int productId)
        {
            return await _repository.Entities.Where(p => p.Id == productId).FirstOrDefaultAsync();
        }

        public async Task<List<Product>> GetListAsync()
        {
            return await _repository.Entities.ToListAsync();
        }

        public async Task<int> InsertAsync(Product product)
        {
            await _repository.AddAsync(product);
            return product.Id;
        }

        public async Task UpdateAsync(Product product)
        {
            await _repository.UpdateAsync(product);
        }


        public async Task<List<Product>> GetFeaturedProductsAsync()
        {

            // implement stored procedure version here
            List<Product> _featuredProducts = new List<Product>();
            var result = await _dbContext.Products.FromSqlRaw(@"exec sp_get_FeaturedProducts").ToListAsync();

            foreach (var row in result)
            {
                _featuredProducts.Add(new Product
                {
                    Id = row.Id,
                    Name = row.Name,
                    Sku = row.Sku,
                    Description = row.Description,
                    Price = row.Price
                });
            }
            return _featuredProducts;
        }

        public async Task<List<Product>> GetProductsByCatgoryAsync(int categoryId)
        {
            return await _repository.Entities.Where(p => p.CategoryId == categoryId).ToListAsync();
        }
    }
}