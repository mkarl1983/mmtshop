﻿using Microsoft.EntityFrameworkCore;
using MMTShop.Domain.Entities;
namespace MMTShop.Infrastructure.DbContexts
{
    public class StoredProceduresDbContext : DbContext
    {
        public StoredProceduresDbContext(DbContextOptions<StoredProceduresDbContext> options) : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }

    }
}