﻿
using System;
using System.Threading.Tasks;

namespace MMTShopWebApiClient
{
    class Program
    {
        static async Task Main(string[] args)
        {

            System.Threading.Thread.Sleep(20000);
            var apiVersion = "1";
            var _webApiBaseUrl = "https://localhost:44377/";

            var webApiClient = new WebApiClient(_webApiBaseUrl, new System.Net.Http.HttpClient());

            // Seed data 

            //await webApiClient.CreateCategoryAsync(version: "1", new CreateCategoryCommand() { Name = "Home", Description= "Home Category" });
            //await webApiClient.CreateCategoryAsync(version: "1", new CreateCategoryCommand() { Name = "Garden", Description = "Garden Category" });
            //await webApiClient.CreateCategoryAsync(version: "1", new CreateCategoryCommand() { Name = "Electronics", Description = "Electronics Category" });
            //await webApiClient.CreateCategoryAsync(version: "1", new CreateCategoryCommand() { Name = "Fitness ", Description = "Fitness Category" });
            //await webApiClient.CreateCategoryAsync(version: "1", new CreateCategoryCommand() { Name = "Toys", Description = "Toys Category" });

            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Home Product1", Description = "Product1 of type Home", Price = 105, CategoryId = 1, Sku = "1xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Home Product2", Description = "Product2 of type Home", Price = 135, CategoryId = 1, Sku = "1xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Home Product3", Description = "Product3 of type Home", Price = 115, CategoryId = 1, Sku = "1xxxx" });

            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Garden Product1", Description = "Product1 of type Garden", Price = 50, CategoryId = 3, Sku = "3xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Garden Product2", Description = "Product2 of type Garden", Price = 15, CategoryId = 3, Sku = "3xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Garden Product3", Description = "Product3 of type Garden", Price = 45, CategoryId = 3, Sku = "3xxxx" });


            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Electronics Product1", Description = "Product1 of type Electronics", Price = 205, CategoryId = 2, Sku = "2xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Electronics Product2", Description = "Product2 of type Electronics", Price = 435, CategoryId = 2, Sku = "2xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Electronics Product3", Description = "Product3 of type Electronics", Price = 65, CategoryId = 2, Sku = "2xxxx" });


            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Fitness Product1", Description = "Product1 of type Fitness", Price = 306, CategoryId = 4, Sku = "4xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Toys Product1", Description = "Product1 of type Toys", Price = 25, CategoryId = 5, Sku = "5xxxx" });
            //await webApiClient.CreateProductAsync(version: "1", new CreateProductCommand() { Name = "Toys Product2", Description = "Product2 of type Toys", Price = 5, CategoryId = 5, Sku = "5xxxx" });



            var featuredProducts = await webApiClient.FeaturedProductsAsync(version: apiVersion);
            var availableCategories = await webApiClient.AvailableCategoriesAsync(version: apiVersion);
            var productsByCategory = await webApiClient.ProductsByCategoryAsync(id: 2, version: apiVersion);



            if (featuredProducts.Succeeded)
            {
                Console.WriteLine("Featured Products...........................");

                foreach (var item in featuredProducts.Data)
                {
                    Console.WriteLine(item.Name);
                }
            }

            if (availableCategories.Succeeded)
            {
                Console.WriteLine("Available Categories...........................");
                foreach (var item in availableCategories.Data)
                {
                    Console.WriteLine(item.Name);
                }
            }

            if (productsByCategory.Succeeded)
            {
                Console.WriteLine("Electronics Products..........................");
                foreach (var item in productsByCategory.Data)
                {
                    Console.WriteLine(item.Name);
                }
            }


        }
    }
}
