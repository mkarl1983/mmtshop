﻿using MMTShop.Application.Features.Categories.Commands.Create;
using MMTShop.Application.Features.Categories.Queries.GetAll;
using MMTShop.Application.Features.Categories.Queries.GetById;
using MMTShop.Domain.Entities;
using AutoMapper;

namespace MMTShop.Application.Mappings
{
    internal class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<CreateCategoryCommand, Category>().ReverseMap();
            CreateMap<GetCategoryByIdResponse, Category>().ReverseMap();
            CreateMap<GetAllCategoryResponse, Category>().ReverseMap();
        }
    }
}