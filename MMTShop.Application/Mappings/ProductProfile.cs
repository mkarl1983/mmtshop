﻿using AutoMapper;
using MMTShop.Application.Features.Products.Queries.GetFeaturedProducts;
using MMTShop.Application.Features.Products.Commands.Create;
using MMTShop.Application.Features.Products.Queries.GetAllPaged;
using MMTShop.Application.Features.Products.Queries.GetById;
using MMTShop.Domain.Entities;

namespace MMTShop.Application.Mappings
{
    internal class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<CreateProductCommand, Product>().ReverseMap();
            CreateMap<GetProductByIdResponse, Product>().ReverseMap();
            CreateMap<GetFeaturedProductsResponse, Product>().ReverseMap();
            CreateMap<GetProductsByCategoryResponse, Product>().ReverseMap();
            CreateMap<GetAllProductsResponse, Product>().ReverseMap();
        }
    }
}