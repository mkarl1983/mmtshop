﻿using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MMTShop.Application.Common;
using MMTShop.Application.Interfaces.CacheRepositories;

namespace MMTShop.Application.Features.Categories.Queries.GetAll
{
    public class GetAllCategoryQuery : IRequest<Result<List<GetAllCategoryResponse>>>
    {
        public GetAllCategoryQuery()
        {
        }
    }

    public class GetAllCategoryCachedQueryHandler : IRequestHandler<GetAllCategoryQuery, Result<List<GetAllCategoryResponse>>>
    {
        private readonly ICategoryCacheRepository _categoryCache;
        private readonly IMapper _mapper;

        public GetAllCategoryCachedQueryHandler(ICategoryCacheRepository categoryCache, IMapper mapper)
        {
            _categoryCache = categoryCache;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllCategoryResponse>>> Handle(GetAllCategoryQuery request, CancellationToken cancellationToken)
        {
            var categoryList = await _categoryCache.GetCachedListAsync();
            var mappedCategories = _mapper.Map<List<GetAllCategoryResponse>>(categoryList);
            return Result<List<GetAllCategoryResponse>>.Success(mappedCategories);
        }
    }
}