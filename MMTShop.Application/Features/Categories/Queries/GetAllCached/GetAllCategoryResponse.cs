﻿namespace MMTShop.Application.Features.Categories.Queries.GetAll
{
    public class GetAllCategoryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}