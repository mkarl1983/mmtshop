﻿using MMTShop.Application.Interfaces.CacheRepositories;

using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using MMTShop.Application.Common;
using System.Collections.Generic;

namespace MMTShop.Application.Features.Products.Queries.GetFeaturedProducts
{
    public class GetProductsByCategoryQuery : IRequest<Result<List<GetProductsByCategoryResponse>>>
    {
        public int Id { get; set; }

        public class GetProductsByCategoryQueryHandler : IRequestHandler<GetProductsByCategoryQuery, Result<List<GetProductsByCategoryResponse>>>
        {
            private readonly IProductCacheRepository _productCache;
            private readonly IMapper _mapper;

            public GetProductsByCategoryQueryHandler(IProductCacheRepository productCache, IMapper mapper)
            {
                _productCache = productCache;
                _mapper = mapper;
            }

            public async Task<Result<List<GetProductsByCategoryResponse>>> Handle(GetProductsByCategoryQuery query, CancellationToken cancellationToken)
            {
                var productList = await _productCache.GetCachedProductsByCategoryListAsync(query.Id);
                var mappedProduct = _mapper.Map<List<GetProductsByCategoryResponse>>(productList);
                return Result<List<GetProductsByCategoryResponse>>.Success(mappedProduct);
            }
        }
    }
}