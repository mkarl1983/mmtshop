﻿namespace MMTShop.Application.Features.Products.Queries.GetAllPaged
{
    public class GetAllProductsResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}