﻿using MMTShop.Application.Interfaces.CacheRepositories;

using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using MMTShop.Application.Common;
using System.Collections.Generic;

namespace MMTShop.Application.Features.Products.Queries.GetFeaturedProducts
{
    public class GetFeaturedProductsQuery : IRequest<Result<List<GetFeaturedProductsResponse>>>
    {
        public int Id { get; set; }

        public class GetFeaturedProductsQueryHandler : IRequestHandler<GetFeaturedProductsQuery, Result<List<GetFeaturedProductsResponse>>>
        {
            private readonly IProductCacheRepository _productCache;
            private readonly IMapper _mapper;

            public GetFeaturedProductsQueryHandler(IProductCacheRepository productCache, IMapper mapper)
            {
                _productCache = productCache;
                _mapper = mapper;
            }

            public async Task<Result<List<GetFeaturedProductsResponse>>> Handle(GetFeaturedProductsQuery query, CancellationToken cancellationToken)
            {
                var productList = await _productCache.GetCachedFeaturedProductListAsync();
                var mappedProduct = _mapper.Map<List<GetFeaturedProductsResponse>>(productList);
                return Result<List<GetFeaturedProductsResponse>>.Success(mappedProduct);
            }
        }
    }
}