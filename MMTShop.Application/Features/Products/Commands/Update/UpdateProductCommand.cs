﻿using MMTShop.Application.Interfaces.Repositories;

using MediatR;
using System.Threading;
using System.Threading.Tasks;
using MMTShop.Application.Common;

namespace MMTShop.Application.Features.Products.Commands.Update
{
    public class UpdateProductCommand : IRequest<Result<int>>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }

        public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, Result<int>>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IProductRepository _productRepository;

            public UpdateProductCommandHandler(IProductRepository productRepository, IUnitOfWork unitOfWork)
            {
                _productRepository = productRepository;
                _unitOfWork = unitOfWork;
            }

            public async Task<Result<int>> Handle(UpdateProductCommand command, CancellationToken cancellationToken)
            {
                var product = await _productRepository.GetByIdAsync(command.Id);

                if (product == null)
                {
                    return Result<int>.Fail($"Product Not Found.");
                }
                else
                {
                    product.Name = command.Name ?? product.Name;
                    product.Price = (command.Price == 0) ? product.Price : command.Price;
                    product.Description = command.Description ?? product.Description;
                    product.CategoryId = (command.CategoryId == 0) ? product.CategoryId : command.CategoryId;
                    await _productRepository.UpdateAsync(product);
                    await _unitOfWork.Commit(cancellationToken);
                    return Result<int>.Success(product.Id);
                }
            }
        }
    }
}