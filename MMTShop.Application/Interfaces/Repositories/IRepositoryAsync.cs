﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMTShop.Application.Interfaces.Repositories
{
    public interface IRepositoryAsync<T> where T : class
    {
        IQueryable<T> Entities { get; }

        Task<T> GetByIdAsync(int id);

        Task<List<T>> GetAllAsync();

        Task<List<T>> GetPagedReponseAsync(int pageNumber, int pageSize);

        Task<T> AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);


        //When you expect a model back (async)
        //Task<IList<T>> ExecWithStoreProcedureAsync(string query, params object[] parameters);

        ////When you expect a model back
        //IEnumerable<T> ExecWithStoreProcedure(string query);

        //// Fire and forget (async)
        //Task ExecuteWithStoreProcedureAsync(string query, params object[] parameters);

        //// Fire and forget
        //void ExecuteWithStoreProcedure(string query, params object[] parameters);



        //When you expect a model back (async)
        //public async Task<IList<T>> ExecWithStoreProcedureAsync<T>(string query, params object[] parameters)
        //{
        //    return await Context.Database.SqlQuery<T>(query, parameters).ToListAsync();
        //}

        ////When you expect a model back
        //public IEnumerable<T> ExecWithStoreProcedure<T>(string query)
        //{
        //    return Context.Database.SqlQuery<T>(query);
        //}

        //// Fire and forget (async)
        //public async Task ExecuteWithStoreProcedureAsync(string query, params object[] parameters)
        //{
        //    await Context.Database.ExecuteSqlCommandAsync(query, parameters);
        //}

        //// Fire and forget
        //public void ExecuteWithStoreProcedure(string query, params object[] parameters)
        //{
        //    Context.Database.ExecuteSqlCommand(query, parameters);
        //}

    }
}