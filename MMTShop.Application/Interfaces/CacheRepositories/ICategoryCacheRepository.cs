﻿using MMTShop.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMTShop.Application.Interfaces.CacheRepositories
{
    public interface ICategoryCacheRepository
    {
        Task<List<Category>> GetCachedListAsync();

        Task<Category> GetByIdAsync(int catgoryId);
    }
}