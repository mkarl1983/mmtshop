﻿using MMTShop.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMTShop.Application.Interfaces.CacheRepositories
{
    public interface IProductCacheRepository
    {
        Task<List<Product>> GetCachedListAsync();

        Task<Product> GetByIdAsync(int productId);

        Task<List<Product>> GetCachedFeaturedProductListAsync();

        Task<List<Product>> GetCachedProductsByCategoryListAsync(int categoryId);
    }
}